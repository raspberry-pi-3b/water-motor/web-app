import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { SignupComponent } from './signup/signup.component';
// import { WeatherComponent } from './weather/weather.component';

const routes: Routes = [
  { path: '', redirectTo: 'home',  pathMatch: 'full', runGuardsAndResolvers: 'always'},
  { path: 'home', component: HomeComponent, runGuardsAndResolvers: 'always'},
  { path: 'signup', component: SignupComponent, runGuardsAndResolvers: 'always'},
  // { path: 'weather', component: WeatherComponent}
];
@NgModule({
  imports: [
    RouterModule.forRoot (routes, {onSameUrlNavigation: 'reload'}),
    CommonModule
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class AppRoutingModule { }
