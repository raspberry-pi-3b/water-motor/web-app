import { Component, OnInit } from '@angular/core';
import { UsersService } from '../services/users/users.service';

export class UserInfoModel {
  constructor (
    public name: string,
    public email: string,
    public phone: string,
    public password: string,
    public retypePassword: string
  ) {}
}

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  userInfoModel: UserInfoModel = new UserInfoModel ("", "", "", "", "");
  title: String = "Sign Up";
  matched: boolean = true;
  error: boolean = false;
  errorMessage: String = "";
  constructor(private usersService: UsersService) { }

  ngOnInit() {

  }

  signupUser () {
    this.resetFlags ();
    if ( this.userInfoModel.password != this.userInfoModel.retypePassword ) {
      this.matched = false;
      return;
    }
    this.usersService.signup (this.userInfoModel)
    .catch ((errorObject) => {
      this.error = true;
      this.errorMessage = errorObject.json().error;
    });
  }

  resetFlags () {
    this.matched = true;
    this.error = false;
    this.errorMessage = "";
  }
}
