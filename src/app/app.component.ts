import { Component, OnInit, OnDestroy } from '@angular/core';
import { UsersService } from './services/users/users.service';
import { Router } from '@angular/router';
import { WeatherService } from './services/weather/weather.service';
declare var jQuery:any;
export class UserInfoModel {
  constructor (
    public name: string,
    public email: string,
    public phone: string,
    public role: string,
    public createdOn: string
  ) {}
}

export class LoginModel {
  constructor (
    public name: string,
    public password: string
  ) {}
}
/*
caption: "Rain"
date: "Fri May 17 2019"
description: "moderate rain"
humidity: 24
icon: "http://api.openweathermap.org/img/w/10d"
max: "36.35 °C"
min: "27.55 °C"
*/

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit, OnDestroy {
  userInfoModel: UserInfoModel = new UserInfoModel ("", "", "", "", "");
  loggedIn: boolean = false;
  currentDate: String = new Date().toString().split(' ').slice(0, 5).join(' ');
  error: boolean = false;
  errorMessage: String = "" ;
  loginModel: LoginModel = new LoginModel ("", "");
  timeUpdate: any;
  currentWeatherJob: any;
  currentWeather: any;
  weatherForecast: any;

  mySlideOptions={
    items: 3, 
    dots: false, 
    nav: true, 
    itemElement: "div", 
    margin: 10, 
    loop: false, 
    responsiveClass: true,
    responsive:{
        0:{
            items:1,
            nav:true,
            loop:false
        },
        // 600:{
        //     items:2,
        //     nav:false,
        //     loop:false
        // },
        1920:{
            items:3,
            nav:true,
            loop:false
        }
    }
  };
  weatherLoaded: Boolean = false;
  constructor (private usersService: UsersService, private router: Router, private weatherService: WeatherService) {
    this.timeUpdate = setInterval(() => {
      this.currentDate = new Date().toString().split(' ').slice(0, 4).join(' ') + ' ' + this.formatAMPM (Date.now());
    }, 1000);
    this.getCurrentWeather();
    this.getForecast();
    this.currentWeatherJob = setInterval (() => {
      this.getCurrentWeather ();
    }, (1 * 60 * 60 * 1000));
  }
  
  ngOnInit () {
    this.getUserInfo ();
  }

  ngOnDestroy () {
    clearInterval (this.timeUpdate);
    clearInterval (this.currentWeatherJob);
  }

  updateUserInfoModel () {
    this.userInfoModel.name = localStorage.getItem ('userName');
    this.userInfoModel.email = localStorage.getItem ('userEmail');
    this.userInfoModel.phone = localStorage.getItem ('userPhone');
    this.userInfoModel.role = localStorage.getItem ('userRole');
    this.userInfoModel.createdOn = localStorage.getItem ('createdOn');
  }

  loginUser () {
    this.resetFlags ();
    this.usersService.login (this.loginModel)
    .then ((response) => {
      if (response) {
        jQuery('#loginDialog').modal('toggle');   
        return this.getUserInfo ();
      }
    }).catch ((errorObject) => {
      this.error = true;
      this.errorMessage = errorObject.json().error;
    });
  }

  resetFlags () {
    this.error = false;
    this.errorMessage = "";
  }

  getUserInfo () {
    if (localStorage.getItem ('token')) {
      var tokenExpiration = new Date (parseInt (localStorage.getItem ('expiration'))).getTime();
      var currentTime = new Date().getTime();
      if (tokenExpiration && currentTime < tokenExpiration) {
        this.loggedIn = true;
      } else {
        localStorage.clear();
        this.loggedIn = false;
      }
      this.updateUserInfoModel();  
    } else this.loggedIn = false; 
    this.router.navigateByUrl('/');  
  }

  logOut () {
    localStorage.clear();
    this.loggedIn = false;
    this.router.navigateByUrl('/');  
  }

  formatAMPM (time) {
    var date = new Date (time);
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var seconds = date.getSeconds();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    var hh = hours < 10 ? '0'+hours : hours ;
    var mm = minutes < 10 ? '0'+minutes : minutes;
    var ss = seconds < 10 ? '0'+seconds : seconds;
    var strTime = hh + ':' + mm + ':' + ss + ' ' + ampm;
    return strTime;
  }

  getCurrentWeather () {
    this.weatherService.getCurrentWeather()
    .then ((response) => {
      this.currentWeather = response.json();
      this.currentWeather.sunrise = this.formatAMPM (this.currentWeather.sunrise);
      this.currentWeather.sunset = this.formatAMPM (this.currentWeather.sunset);
      // console.log (this.currentWeather);
    })
    .catch ((errorObject) => {
      this.error = true;
      this.errorMessage = errorObject.json().error;
    });
  }

  getForecast () {
    this.weatherService.getForecast()
    .then ((response) => {
      this.weatherForecast = response.json(); //Get an array of size 5
      for (let i = 0; i < this.weatherForecast.length; i++) {
        this.weatherForecast [i].date = new Date(this.weatherForecast [i].date).toString().split(' ').slice(0, 4).join(' ');        
      }
      this.weatherLoaded = true;
    })
    .catch ((errorObject) => {
      this.error = true;
      this.errorMessage = errorObject.json().error;
    });
  }
}
