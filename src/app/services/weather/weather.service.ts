import { Injectable } from '@angular/core';
import { Http, Jsonp, Response, Headers, RequestOptions } from '@angular/http';
import { Router } from '@angular/router';
import { Globals } from '../../../../global';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {
  http: Http;
  constructor(http: Http, private router: Router) {
      this.http = http;
  }

  public getCurrentWeather () {
    var url = Globals.ADMIN_BASE_API_URL + '/weather/getCurrentWeather';
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    // headers.append('Authorization', 'bearer '+ localStorage.getItem ('token'));
    // var body = {motorStatus: motorStatus};
    // return this.http.post (url, body, {headers})
    return this.http.get (url, {headers})
    .toPromise()
    .then((response: Response) => {
      return response;
    })
    .catch ( (error) => {
      throw error;
    });
  }

  public getForecast () {
    var url = Globals.ADMIN_BASE_API_URL + '/weather/getForecast';
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    // headers.append('Authorization', 'bearer '+ localStorage.getItem ('token'));
    // var body = {motorStatus: motorStatus};
    // return this.http.post (url, body, {headers})
    return this.http.get (url, {headers})
    .toPromise()
    .then((response: Response) => {
      return response;
    })
    .catch ( (error) => {
      throw error;
    });
  }
}
