import { Injectable } from '@angular/core';
import { Http, Jsonp, Response, Headers, RequestOptions } from '@angular/http';
import { Router } from '@angular/router';
import { Globals } from '../../../../global';
import { debug } from 'util';


@Injectable({
  providedIn: 'root'
})
export class UsersService {
  http: Http;
  constructor(http: Http, private router: Router) {
      this.http = http;
  }

  public signup (signupModel) {
    var url = Globals.ADMIN_BASE_API_URL + '/users/addNewUser';
    var body = {
        name: signupModel.name,
        password: signupModel.password,
        retypePassword: signupModel.retypePassword, 
        email: signupModel.email,
        phone: signupModel.phone,
    };
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post (url, body, {headers})
    .toPromise()
    .then((response: Response) => {
      this.router.navigate(['/home']);
    })
    .catch ( (error) => {
      throw error;
    })
  }

  public login (loginModel): Promise<Object> {
    var url = Globals.BASE_API_URL + '/users/loginUser';
    var body = {
        name: loginModel.name,
        password: loginModel.password
    };
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post (url, body, {headers})
    .toPromise()
    .then  ((response: Response) => {
        if (this.saveJwt (response)) return response;
    });
  }

  saveJwt (response) {
    if (response) {
      try {
        var response = response.json();
        localStorage.setItem ('token', response.token);
        localStorage.setItem ('expiration', response.expiration);
        localStorage.setItem ('userName', response.data.name);
        localStorage.setItem ('userEmail', response.data.email);
        localStorage.setItem ('userPhone', response.data.phone);
        localStorage.setItem ('userRole', response.data.role);
        localStorage.setItem ('createdOn', response.data.createdOn);
        return true;
      } catch (error) {
        throw error;
      }
    }
  }

  // public MakeVote (body) {
  //   var url = Globals.ADMIN_BASE_API_URL + '/actions/addVote';
  //   var headers = new Headers();
  //   headers.append('Content-Type', 'application/json');
  //   headers.append('Authorization', 'bearer '+localStorage.getItem('token'));
  //   return this.http.post (url, body, {headers})
  //   .toPromise()
  //   .then((response: Response) => response);
  // }
}
