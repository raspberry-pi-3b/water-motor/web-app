import { Injectable } from '@angular/core';
import { Http, Jsonp, Response, Headers, RequestOptions } from '@angular/http';
import { Router } from '@angular/router';
import { Globals } from '../../../../global';

@Injectable({
  providedIn: 'root'
})
export class HomeService {
  http: Http;
  constructor(http: Http, private router: Router) {
      this.http = http;
  }

  public getSystemStatus () {
    var url = Globals.ADMIN_BASE_API_URL + '/home/getSystemStatus';
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get (url, {headers})
    .toPromise()
    .then((response: Response) => {
      return response;
    })
    .catch ( (error) => {
      throw error;
    });
  }

  public motorSwitch (motorStatus) {
    var url = Globals.ADMIN_BASE_API_URL + '/home/motorSwitch';
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', 'bearer '+ localStorage.getItem ('token'));
    var body = {motorStatus: motorStatus};
    return this.http.post (url, body, {headers})
    .toPromise()
    .then((response: Response) => {
      return response;
    })
    .catch ( (error) => {
      throw error;
    });
  }
}
