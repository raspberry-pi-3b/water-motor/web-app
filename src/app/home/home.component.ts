//https://www.fusioncharts.com/dev/getting-started/angular/angular/configure-your-chart-using-angular#update-chart-attributes
//https://www.fusioncharts.com/dev/getting-started/angular/angular/configure-your-chart-using-angular
//https://www.google.com/search?rlz=1C1CHZL_enPK814PK814&ei=SA_UXKTYJY_gUYjqnuAI&q=fusionchart+chart+update+angular&oq=fusionchart+chart+update+angular&gs_l=psy-ab.3..0i71l8.8825.9386..9503...0.0..0.323.935.2-1j2......0....1..gws-wiz.PMe1cjAFhX4

import { Component, OnInit, OnDestroy } from '@angular/core';
import { UsersService } from '../services/users/users.service';
import { HomeService } from '../services/home/home.service';
import { Router, NavigationEnd } from '@angular/router';

export class UserInfoModel {
  constructor (
    public name: string,
    public email: string,
    public phone: string,
    public role: string,
    public createdOn: string
  ) {}
}

export class LoginModel {
  constructor (
    public name: string,
    public password: string
  ) {}
}

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit, OnDestroy {
  // Fusionchart start
  roofTank: any;
  groundTank: any;

  motorStatus: boolean = false;

  title = 'Tank Level & Motor Control';
  loginModel: LoginModel = new LoginModel ("", "");
  loggedIn: boolean;
  userInfoModel: UserInfoModel = new UserInfoModel ("", "", "", "", "");
  navigationSubscription;
  sysUpdate;
  error: boolean = false;
  errorMessage: String = '';

  motorError: boolean = false;
  motorErrorMessage: String = "";

  constructor(private usersService: UsersService, private homeService: HomeService, private router: Router) {
    this.roofTank = {
      "chart": {
        "theme": "Fusion",
        "bgColor": "#343A40",
        "bgAlpha": "100",
        "showBorder":"0",
        "baseFontColor":"#00FFFF",
        "baseFontSize":"16",
        "cylFillColor":"#0077be",
        "valuePadding":"30",
        "caption": "Roof Tank",
        "captionFontColor":"#00FFFF",
        "subcaption": "",
        "lowerLimit": "0",
        "upperLimit": "7200",
        "lowerLimitDisplay": "Empty",
        "upperLimitDisplay": "",
        "numberSuffix": " ltrs",
        "showValue": "1",
        "chartBottomMargin": "25"
      },
      "value": 1500
    }

    this.groundTank = {
      "chart": {
        "theme": "Fusion",
        "bgColor": "#343A40",
        "bgAlpha": "100",
        "showBorder":"0",
        "baseFontColor":"#00FFFF",
        "baseFontSize":"16",
        "cylFillColor":"#0077be",
        "valuePadding":"30",
        "caption": "Ground Tank",
        "captionFontColor":"#00FFFF",
        "subcaption": "",
        "lowerLimit": "0",
        "upperLimit": "2350",
        "lowerLimitDisplay": "Empty",
        "upperLimitDisplay": "",
        "numberSuffix": " ltrs",
        "showValue": "1",
        "chartBottomMargin": "25"
      },
      "value": 500
    }    
    this.navigationSubscription = this.router.events.subscribe((e: any) => {
      // If it is a NavigationEnd event re-initalise the component
      if (e instanceof NavigationEnd) {
        this.getUserInfo();
      }
    });
  }  

  ngOnInit () {
    this.getUserInfo ();    
    this.sysUpdate = setInterval (() => {
      this.homeService.getSystemStatus ()
      .then ((response) => {
        if (response) {
          this.error = false;
          this.errorMessage = "";
          let values = response.json();
          this.groundTank.value = values.ground;
          this.roofTank.value = values.roof;
          this.motorStatus = (values.motorStatus=="off")? false:true;
          // console.log (this.motorStatus);
        }
      })
      .catch ((errorObject) => {
        this.error = true;
        this.errorMessage = errorObject.json().error;
      });
    }, 10000);
  }
  
  ngOnDestroy () {
    this.navigationSubscription.unsubscribe();
    clearInterval(this.sysUpdate);
  }

  switchMotor () {   
    this.resetFlags (); 
    this.homeService.motorSwitch (this.motorStatus? false:true)
    .then ((response) => {
      if (response) {
        debugger;
        // console.log (response);
        this.resetFlags ();
        this.motorStatus = this.motorStatus? false:true;
      }
    })
    .catch ((errorObject) => {
      debugger;
      this.motorError = true;
      this.motorErrorMessage = errorObject.json().error;
    })
  }

  getUserInfo () {
    if (localStorage.getItem ('token')) {
      var tokenExpiration = new Date (parseInt (localStorage.getItem ('expiration'))).getTime();
      var currentTime = new Date().getTime();
      if (tokenExpiration && currentTime < tokenExpiration) {
        this.loggedIn = true;
      } else {
        localStorage.clear();
        this.loggedIn = false;
      }
      this.updateUserInfoModel();  
    } else this.loggedIn = false; 
  }

  updateUserInfoModel () {
    this.userInfoModel.name = localStorage.getItem ('userName');
    this.userInfoModel.email = localStorage.getItem ('userEmail');
    this.userInfoModel.phone = localStorage.getItem ('userPhone');
    this.userInfoModel.role = localStorage.getItem ('userRole');
    this.userInfoModel.createdOn = localStorage.getItem ('createdOn');
  }

  resetFlags () {
    this.error = false;
    this.errorMessage = "";
    this.motorError = false;
    this.motorErrorMessage = "";
  }
}
